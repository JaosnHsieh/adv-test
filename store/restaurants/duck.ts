import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../index';

export type Restaurant = {
  _id: string;
  name: string;
};
const FETCH_RESTAURANTS_REQUEST = 'FETCH_RESTAURANTS_REQUEST';
const FETCH_RESTAURANTS_SUCCESS = 'FETCH_RESTAURANTS_SUCCESS';
const FETCH_RESTAURANTS_FAILURE = 'FETCH_RESTAURANTS_FAILURE';

export function fetchRestaurantsRequest() {
  return {
    type: FETCH_RESTAURANTS_REQUEST,
  };
}
export function fetchRestaurantsSuccess({
  restaurants,
}: {
  restaurants: Restaurant[];
}) {
  return {
    type: FETCH_RESTAURANTS_SUCCESS,
    payload: restaurants,
  };
}

export function fetchRestaurantsFailure({ errors }: { errors: string[] }) {
  return {
    type: FETCH_RESTAURANTS_FAILURE,
    errors,
  };
}

interface FetchRestaurantsRequestAction {
  type: typeof FETCH_RESTAURANTS_REQUEST;
  // payload: Restaurant[];
}

interface FetchRestaurantsSuccessAction {
  type: typeof FETCH_RESTAURANTS_SUCCESS;
  payload: Restaurant[];
}

interface FetchRestaurantsFailureAction {
  type: typeof FETCH_RESTAURANTS_FAILURE;
  errors: string[];
  // payload: Restaurant[];
}

export type RestaurantActionTypes =
  | FetchRestaurantsRequestAction
  | FetchRestaurantsSuccessAction
  | FetchRestaurantsFailureAction;

export interface RestaurantState {
  data: Restaurant[];
  isLoading: boolean;
  errors: string[];
}

const initialState: RestaurantState = {
  data: [],
  isLoading: false,
  errors: [],
};

export function restaurantsReducer(
  state = initialState,
  action: RestaurantActionTypes,
): RestaurantState {
  switch (action.type) {
    case FETCH_RESTAURANTS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_RESTAURANTS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
      };
    case FETCH_RESTAURANTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        errors: action.errors,
      };
    default:
      return state;
  }
}

export const thunkFetchRestaurant = (options?: {
  openingTimeStamp: number;
}): ThunkAction<void, RootState, null, Action<string>> => async dispatch => {
  try {
    dispatch(fetchRestaurantsRequest());

    const res = await fetch(
      `/api/restaurants${
        options?.openingTimeStamp
          ? '?openingTimeStamp=' + options.openingTimeStamp
          : ''
      }`,
    );
    if (res.ok) {
      const data = await res.json();
      console.log('data', data);
      return dispatch(
        fetchRestaurantsSuccess({ restaurants: data as Restaurant[] }),
      );
    }
  } catch (err) {
    dispatch(fetchRestaurantsFailure({ errors: ['unexpected fetch error'] }));
  }
};
