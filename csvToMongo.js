require('dotenv').config();
const path = require('path');
const csvFilePath = path.join(__dirname, './japan-restaurants.csv');
const csv = require('csvtojson');
const mongoose = require('mongoose');
const { Restaurant } = require('./server/db/models');
mongoose
  .connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const isClosed = str => /closed/gi.test(str);
    const getOpen = str =>
      isClosed(str)
        ? null
        : str.split('-')[0].length === 2
        ? str.split('-')[0] + ':00'
        : str.split('-')[0];
    const getClosed = str =>
      isClosed(str)
        ? null
        : str.split('-')[1].length === 2
        ? str.split('-')[1] + ':00'
        : str.split('-')[1];

    const jsonArray = await csv().fromFile(csvFilePath);
    console.log(`jsonArray`, jsonArray);
    for (const row of jsonArray) {
      const { name, startTime, endTime } = row;
      let openClosedColumns = {};
      ['1', '2', '3', '4', '5', '6', '7'].map(n => {
        openClosedColumns[`day${n}OpenDateString`] = getOpen(row[n]);
        openClosedColumns[`day${n}ClosedDateString`] = getClosed(row[n]);
        openClosedColumns[`isDay${n}Closed`] = isClosed(row[n]);
      });

      const r = new Restaurant({
        name: row['name'],
        ...openClosedColumns,
        type: row['type'],
        star: row['star'] === '有' ? true : false,
        hasParking: row['park'] === '有' ? true : false,
        hasTakeaway: row['hasTakeaway'] === '有' ? true : false,
        hasDeposit: row['hasDeposit'] === '有' ? true : false,
        reviews: row['reviews'] || 0,
        point: row['point'],
      });
      await r.save();
      console.log(`row saved`, row);
    }
  })
  .catch(err => {
    console.log('mongo err', err);
  });
