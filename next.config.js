require('dotenv').config();

module.exports = {
  target: 'server',
  // distDir: '.next',
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    };

    return config;
  },
  env: {},
  serverRuntimeConfig: {
    COOKIE_PARSER_SECRET:
      process.env.COOKIE_PARSER_SECRET || 'cookieparsersecret',
    PORT: process.env.PORT || 3000,
    IS_DEV: process.env.NODE_ENV !== 'production',
    MONGODB_URL: process.env.MONGODB_URL,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    ROOT_URL: process.env.ROOT_URL || 'http://avldev.sharemytube.com:3000',
    ADMIN_USER: process.env.ADMIN_USER || 'admin',
    ADMIN_PASSWORD: process.env.ADMIN_PASSWORD || 'admin',
  },
};
