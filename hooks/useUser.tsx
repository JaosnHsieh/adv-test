import * as React from 'react';

export const useUser = () => {
  const [isLoading, setIsLoading] = React.useState<boolean>(true);
  const [isSignedIn, setIsSignedIn] = React.useState<boolean>(false);
  const [data, setData] = React.useState<{
    displayName?: string;
    _id?: string;
  }>({});

  React.useEffect(() => {
    const getIsSignedIn = async () => {
      const response = await fetch('/me');
      setIsLoading(false);
      if (response.ok) {
        const data = await response.json();
        setData(data);
        setIsSignedIn(true);
      }
    };
    getIsSignedIn();
  }, []);

  return { isLoading, isSignedIn, data };
};
