/**
 * refer to "guide-node-js-logging"
 * https://www.twilio.com/blog/guide-node-js-logging
 * in order to separate express server side and next.js render server and client
 * use pino on express server apis, use debug on next.js render files
 */
const pino = require('pino');
const expressPino = require('express-pino-logger');

const logger = pino({ level: process.env.LOG_LEVEL || 'info' });

const expressLogger = expressPino({ logger });

module.exports = {
  logger,
  expressLogger,
};
