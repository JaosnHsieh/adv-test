const _ = require('lodash');
const nextConfig = require('../next.config');
const debug = require('debug')('AVL-DEMO:server/db/api.js');
const { User, Restaurant } = require('./db');
const moment = require('moment');
require('moment-timezone');

const errors = {
  400: 'fields error',
  403: 'Forbidden',
};

/**
 * @param {Object}  options - A string param.
 * @param {import("express").Application} options.server
 * @param {import("next").Server} options.nextApp
 */
module.exports = ({ server, nextApp }) => {
  server.get('/api/restaurants', async (req, res, next) => {
    try {
      const { openingTimeStamp } = req.query;
      debug(`openingTimeStamp`, openingTimeStamp);
      let currentMoment;
      if (openingTimeStamp) {
        currentMoment = moment.tz(
          parseInt(openingTimeStamp, 10),
          'Asia/Taipei',
        );
      } else {
        currentMoment = moment.tz(new Date(), 'Asia/Taipei');
      }
      const dayOfWeek = currentMoment.day();
      const isOpenNow = restaurant => {
        if (restaurant[`isDay${dayOfWeek}Closed`]) {
          return false;
        }

        const startMoment = moment.tz(
          currentMoment.format(
            `YYYY-MM-DD ${restaurant[`day${dayOfWeek}OpenDateString`]}:ss`,
          ),
          'Asia/Taipei',
        );
        const endMoment = moment.tz(
          currentMoment.format(
            `YYYY-MM-DD ${restaurant[`day${dayOfWeek}ClosedDateString`]}:ss`,
          ),
          'Asia/Taipei',
        );
        return currentMoment.isBetween(startMoment, endMoment);
      };

      const restaurants = await Restaurant.find({});
      return res.json(
        restaurants.filter(isOpenNow).map(({ _id, name }) => ({ _id, name })),
      );
    } catch (err) {
      next(err);
    }
  });
};
