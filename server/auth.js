const _ = require('lodash');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const nextConfig = require('../next.config');
const { User } = require('./db');
const debug = require('debug')('AVL-DEMO:authServer.js');

const {
  serverRuntimeConfig: {
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    ROOT_URL,
    ADMIN_PASSWORD,
    ADMIN_USER,
  },
} = nextConfig;

const insertUser = async ({ name, email, displayName, googleId }) => {
  const user = new User({
    name,
    email,
    displayName,
    googleId,
  });
  await user.save();
  return user;
};

const queryUserByGoogleId = async ({ googleId }) => {
  return User.findOne({ googleId });
};

const googleSignInOrSignUp = async ({
  name = '',
  email = '',
  displayName = '',
  role = 'user',
  googleId = '',
}) => {
  const user = await queryUserByGoogleId({ googleId });
  if (user) {
    return {
      user,
      isOldUser: true,
    };
  }
  const newUser = await insertUser({
    name,
    email,
    displayName,
    googleId,
  });
  return {
    user: newUser,
    isOldUser: false,
  };
};

/**
 * @param {Object}  options - A string param.
 * @param {import("express").Application} options.server
 * @param {import("next").Server} options.nextApp
 */
module.exports = ({ server, nextApp }) => {
  server.use(passport.initialize());

  const googleVerify = async (accessToken, refreshToken, profile, verified) => {
    let name;
    let email;
    let googleId;
    let avatarUrl;
    if (profile.photos && profile.photos.length > 0) {
      avatarUrl = profile.photos[0].value.replace('sz=50', 'sz=128');
    }

    if (profile.id) {
      googleId = profile.id;
    }

    if (profile.displayName) {
      name = profile.displayName;
    }
    if (profile.emails) {
      email = profile.emails[0].value;
    }

    try {
      const { user, isOldUser } = await googleSignInOrSignUp({
        name,
        email,
        displayName: name,
        googleId,
      });
      verified(null, user);
    } catch (err) {
      verified(err);
    }
  };
  passport.use(
    new GoogleStrategy(
      {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: `${ROOT_URL}/google/callback`,
        userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo',
      },
      googleVerify,
    ),
  );

  server.get(
    '/auth/google',
    passport.authenticate('google', {
      session: false,
      scope: ['profile', 'email'],
      prompt: 'select_account',
    }),
  );

  server.get(
    '/google/callback',
    passport.authenticate('google', {
      failureRedirect: '/login',
      session: false,
    }),
    async (req, res) => {
      debug('redirect successfully req.user.id ', req.user.id);
      req.session.user = req.user;
      res.redirect('/');
    },
  );

  server.get('/me', (req, res) => {
    if (req.session.user) {
      return res.json(req.session.user);
    }
    return res.status(404).send('NOT FOUND');
  });

  server.get('/logout', (req, res) => {
    delete req.session.user;
    return res.redirect('/');
  });

  server.post(
    '/login',

    async (req, res) => {
      const { username, password } = req.body;
      if (username && password) {
        if (username === ADMIN_USER && password === ADMIN_PASSWORD) {
          req.session.user = {
            _id: 'admin',
            displayName: 'Admin ',
          };
          return res.redirect('/');
        }
      }
      return res.redirect('/');
    },
  );
};
