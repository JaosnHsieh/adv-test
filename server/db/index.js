const _ = require('lodash');
const nextConfig = require('../../next.config');
const mongoose = require('mongoose');
const { User, Restaurant } = require('./models');
const debug = require('debug')('AVL-DEMO:server/db/index.js');
const {
  serverRuntimeConfig: { MONGODB_URL },
} = nextConfig;

debug(`MONGODB_URL`, MONGODB_URL);
mongoose
  .connect(MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    debug('mongo success');
  })
  .catch(err => {
    debug('mongo err', err);
  });

module.exports = {
  User,
  Restaurant,
};
