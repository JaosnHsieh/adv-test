const mongoose = require('mongoose');
const User = mongoose.model('User', {
  name: String,
  email: String,
  displayName: String,
  facebookId: String,
  googleId: String,
});

const Restaurant = mongoose.model('Restaurant', {
  name: String,
  day7OpenDateString: String,
  day7ClosedDateString: String,
  isDay7Closed: Boolean,
  day6OpenDateString: String,
  day6ClosedDateString: String,
  isDay6Closed: Boolean,
  day5OpenDateString: String,
  day5ClosedDateString: String,
  isDay5Closed: Boolean,

  day4OpenDateString: String,
  day4ClosedDateString: String,
  isDay4Closed: Boolean,

  day3OpenDateString: String,
  day3ClosedDateString: String,
  isDay3Closed: Boolean,

  day2OpenDateString: String,
  day2ClosedDateString: String,
  isDay2Closed: Boolean,

  day1OpenDateString: String,
  day1ClosedDateString: String,
  isDay1Closed: Boolean,
  type: String,
  star: String,
  hasParking: Boolean,
  hasTakeaway: Boolean,
  hasDeposit: Boolean,
  reviews: Number,
  point: String,
});

module.exports = {
  User,
  Restaurant,
};
