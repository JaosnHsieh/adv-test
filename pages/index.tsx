import { WithCsr } from '../hocs/WithCsr';
import * as React from 'react';
import { thunkFetchRestaurant, Restaurant } from '../store/restaurants/duck';
import { RootState } from '../store';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/Table';
import { DatePicker } from 'antd';
import moment from 'moment-timezone';
import { useUser } from '../hooks/useUser';

export interface Home {
  restaurants: Restaurant[];
  fetchRestaurants: (options?: { openingTimeStamp?: number }) => void;
  isRestaurantsLoading: boolean;
}

const Home: React.FC<Home> = ({
  restaurants,
  fetchRestaurants,
  isRestaurantsLoading,
}) => {
  const user = useUser();
  React.useEffect(() => {
    fetchRestaurants();
  }, []);

  function onOk(value) {
    const selectedTimeStamp = moment
      .tz(value.format('YYYY-MM-DD HH:mm:ss'), 'Asia/Taipei')
      .valueOf();
    fetchRestaurants({ openingTimeStamp: selectedTimeStamp });
  }

  return (
    <div>
      <div>
        {user.isSignedIn ? (
          <>
            <div>
              <a href="/logout">logout</a>
            </div>
            Hi, {user?.data?.displayName ?? ''}
          </>
        ) : (
          <h3>
            <a href="/auth/google">Google Sign in</a>
          </h3>
        )}
      </div>
      <hr></hr>
      <h2>Current Open Restaurants:</h2>
      <h3>Select Opening hours:</h3>
      <DatePicker showTime placeholder="Select Time" onOk={onOk} />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {restaurants.map(r => {
            return (
              <tr key={r._id}>
                <td>{r.name}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  restaurants: state.restaurants.data,
  isRestaurantsLoading: state.restaurants.isLoading,
});

export default WithCsr(
  connect(mapStateToProps, {
    fetchRestaurants: thunkFetchRestaurant,
  })(Home),
);
