import * as React from 'react';
import { WithCsr } from '../hocs/WithCsr';
import { useUser } from '../hooks/useUser';
import { RootState } from '../store';
import { connect } from 'react-redux';
import { thunkFetchRestaurant, Restaurant } from '../store/restaurants/duck';

export interface AdminProps {
  restaurants: Restaurant[];
  fetchRestaurants: (options?: { openingTimeStamp?: number }) => void;
  isRestaurantsLoading: boolean;
}
const Admin: React.FC<AdminProps> = () => {
  const [username, setUsername] = React.useState<string>('');
  const [password, setPassword] = React.useState<string>('');
  const user = useUser();

  if (user.isLoading) {
    return <>{'Loading...'}</>;
  }
  if (!user.isLoading && user.isSignedIn && user.data._id === 'admin') {
    return <h2>logined admin, show table</h2>;
  }

  return (
    <div>
      <form action="/login" method="POST">
        <div>
          <input
            name="username"
            placeholder="username"
            className="input"
            value={username}
            onChange={e => {
              setUsername(e.target.value);
            }}
            required
          />
        </div>
        <div>
          <input
            name="password"
            placeholder="password"
            className="input"
            value={password}
            onChange={e => {
              setPassword(e.target.value);
            }}
            type="password"
            required
          />
        </div>
        <input className="submit-btn" type="submit" value="Login" />
      </form>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  restaurants: state.restaurants.data,
  isRestaurantsLoading: state.restaurants.isLoading,
});

export default WithCsr(
  connect(mapStateToProps, {
    fetchRestaurants: thunkFetchRestaurant,
  })(Admin),
);
