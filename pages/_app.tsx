import '../styles/login.css';
import * as React from 'react';
import { configureStore } from '../store';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';

function MyApp({
  Component,
  pageProps,
}: {
  Component: React.FC<unknown>;
  pageProps: { [key in string]: any };
}) {
  return (
    <Provider store={configureStore()}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
