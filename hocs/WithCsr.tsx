import dynamic from 'next/dynamic';

export const WithCsr = Component =>
  dynamic(
    () =>
      new Promise(resolve => {
        resolve(Component);
      }),
    { ssr: false },
  );
