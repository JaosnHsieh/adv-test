const server = require('express')();
const next = require('next');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser');
const { expressLogger, logger } = require('./server/logger');
const nextConfig = require('./next.config');
const api = require('./server/api');
const auth = require('./server/auth');

const {
  serverRuntimeConfig: { PORT: port, IS_DEV: isDev, COOKIE_PARSER_SECRET },
} = nextConfig;

const app = next({ dev: isDev, dir: __dirname, conf: nextConfig });

const handle = app.getRequestHandler();

app.prepare().then(() => {
  server.use(cookieParser(COOKIE_PARSER_SECRET));
  server.use(bodyParser.json()); // to support JSON-encoded bodies
  server.use(
    bodyParser.urlencoded({
      // to support URL-encoded bodies
      extended: true,
    }),
  );
  server.use(
    cookieSession({
      name: 'session',
      keys: ['cookie-session-keys'],
      // Cookie Options
      maxAge: 24 * 60 * 60 * 1000, // 24 hours
    }),
  );
  api({ server, nextAp: app });
  auth({ server, nextApp: app });

  server.get('*', (req, res) => handle(req, res));
  // eslint-disable-next-line no-unused-vars
  server.use((err, req, res, _next) => {
    logger.error(err.stack);
    res.status(500).send('Something broke!');
  });
  server.listen(port, err => {
    if (err) throw err;
    logger.info(`> Ready on http://localhost:${port}`);
  });
});
